### A Pluto.jl notebook ###
# v0.14.1

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ efd3a208-9d1b-11eb-12be-63c9e5ac38f3
using CairoMakie, DifferentialEquations, PlutoUI, Images

# ╔═╡ 923cf5fc-e402-486d-9354-4b6c1ecba79c
begin
	const g = 2.0
	H(θ::Real,p::Real,params) = p^2/2 - sin(θ)
end

# ╔═╡ 7693f8b4-f394-487c-b517-df716ca725c4
begin
	p₀ = 1.
	θ₀ = 0.
	T = (0., 10.)
	dt=0.1
end

# ╔═╡ bcb1d912-62fc-49c8-921b-7bbf1d4f8f50
prob = HamiltonianProblem(H, p₀, θ₀, T)

# ╔═╡ a52ffca4-b9a3-45d7-b699-af1e00e19e1a
sol = solve(prob, DPRKN6(), dt = dt)

# ╔═╡ e94db932-65bc-425e-8bf2-d272cab1124d
begin
	cartesian(θ::Real) = (sin(θ), -cos(θ))
end

# ╔═╡ 8464c59c-cfa7-4448-a02d-f8a32059a247
function pendulum_plot(θ::Real,p::Real)
	scatter(cartesian(θ), limits = FRect(-1, -1, 1, 1))
end

# ╔═╡ 104613f1-2a9d-4869-8645-aa67ca67975b
@bind θt PlutoUI.Slider(0:dt:2* π)

# ╔═╡ 71097abd-abb3-492c-8703-1464e5d97b2e
begin
p= scatter(cartesian(8))
	xlims!(-1.,1.)
	p
end

# ╔═╡ 2c3e85a6-6a54-46a0-bb30-af13d566c215
begin
	p0 = pendulum_plot(sol(0.)[:]...)
	record(p0, "pendulum.gif",T[1]:dt:T[2]; framerate = round(Int,1/dt)) do t
    p0 = pendulum_plot(sol(t)[:]...)
	end
end

# ╔═╡ Cell order:
# ╠═efd3a208-9d1b-11eb-12be-63c9e5ac38f3
# ╠═923cf5fc-e402-486d-9354-4b6c1ecba79c
# ╠═7693f8b4-f394-487c-b517-df716ca725c4
# ╠═bcb1d912-62fc-49c8-921b-7bbf1d4f8f50
# ╠═a52ffca4-b9a3-45d7-b699-af1e00e19e1a
# ╠═e94db932-65bc-425e-8bf2-d272cab1124d
# ╠═8464c59c-cfa7-4448-a02d-f8a32059a247
# ╠═104613f1-2a9d-4869-8645-aa67ca67975b
# ╠═71097abd-abb3-492c-8703-1464e5d97b2e
# ╠═2c3e85a6-6a54-46a0-bb30-af13d566c215

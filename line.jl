using GLMakie
fps = 24
dt = 1. / fps/ 2
ts = 0:dt:2
bgc = RGBf0(0.98, 0.98, 0.98)

Φ = MathConstants.golden *1.

s = Node{Float32}(0.0)

xf(t) = Φ^(2*π*(- t)) * cos(2*π*t)
yf(t) = Φ^(2*π*(- t)) * sin(2*π*t) 
zf(t) = 2*t-1

xs = xf.(ts)
ys = yf.(ts)
zs = zf.(ts)

text_position = @lift string("s = ",  floor($s, digits = 1))

point2 = @lift Point2(xf($s), yf($s))
point3 = @lift Point3(xf($s), yf($s), zf($s))


figure = Figure(backgroundcolor = bgc)
ax2 = figure[1,1] = Axis(figure )
ax2.limits =((-1,+1),(-1,+1))

lines!(ax2, xs,ys, color =:black)
scatter!(ax2, point2, color =:orange)



ax3 = figure[1,2] = Axis3(figure, aspect = (1,1,1), xlabel = "",ylabel = "", zlabel = "" )
ax3.limits =((-1,+1),(-1,+1),(-1,+1))

lines!(ax3, xs,ys,zs, color =:black)

bigpoint= @lift Sphere($point3, .05)
mesh!(ax3, bigpoint,color = :orange)

label= figure[2,:] = Label(figure, text_position, textsize = 30, font = "Fira Mono")


position_iter1 = Iterators.flatten((0. : dt : 1.,
 1. : -dt : 0.))
record(figure, "line.gif", position_iter1,  framerate = fps) do t
        s[] = t
end

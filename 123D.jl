using GLMakie
fps = 24
dt = 1. / fps
dθ = dt * π / 2
# position = Node{Vec3f0(0.,0.,0.)}
x = Node{Float32}(0.0)
y = Node{Float32}(0.0)
z = Node{Float32}(0.0)

bgc = RGBf0(0.98, 0.98, 0.98)

point1 = @lift Point2($x,0)
point2 = @lift Point2($x,$y)
point3 = @lift Point3($x,$y,$z)

text1 = @lift string(
    "x = ", floor($x, digits = 1))
    text2 = @lift string(
        "x = ", floor($x, digits = 1), ", ",
        "y = ", floor($y, digits = 1), ", "
        )
        text3 = @lift string(
            "x = ", floor($x, digits = 1), ", ",
            "y = ", floor($y, digits = 1), ", ",
            "z = ", floor($z, digits = 1)
            )
            
            
θ = Node{Float32}(0.0)
ϕ = Node{Float32}(0.0)
sphere_point = @lift Point3(cos($ϕ) * sin($θ), sin($ϕ) * sin($θ), cos($θ))
θs = 0:dθ:π
ϕs = 0:dθ:2π

xs  = [cos(ϕ) * sin(θ) for θ in θs, ϕ in ϕs]
ys  = [sin(ϕ) * sin(θ) for θ in θs, ϕ in ϕs]
zs  = [cos(θ) for θ in θs, ϕ in ϕs]

text_shpere = @lift string(
    "θ = ", lpad(floor(Int, $θ *180. /π),3), "°, ",
    "ϕ = ", lpad(floor(Int, $ϕ *180. /π),3), "°")


## 1D
figure1 = Figure(backgroundcolor = bgc)
ax1 = Axis(figure1[1,1], aspect = AxisAspect(1), xminorticksvisible =true, xminorgridvisible = true)
hideydecorations!(ax1)
limits!(ax1,-0.1, 1.1, -1, 1)
scatter!(ax1, point1, color = :orange)
hidespines!(ax1, :t, :r, :l)

label1 =figure1[2,:] = Label(figure1, text1, textsize = 30, font = "Fira Mono")

position_iter1 = Iterators.flatten((0. : dt : 1.,
 1. : -dt : 0.))
record(figure1, "1d.gif", position_iter1,  framerate = fps) do t
        x[] = t
end

## 2D
figure2 = Figure(backgroundcolor = bgc,)
ax2 = Axis(figure2[1,1], aspect = AxisAspect(1))
limits!(ax2,-0.1, 1.1, -0.1, 1.1)
scatter!(ax2, point2, color = :orange)

label2 = figure2[2,:] = Label(figure2, text2, textsize = 30, font = "Fira Mono")

position_iter2 = Iterators.flatten((
    Iterators.product(0:dt:1,1:2),
    Iterators.product(1:-dt:0,1:2) ))

record(figure2, "2d.gif", position_iter2, framerate = fps) do (t,i)
    if i == 1
        x[] = t
    elseif i == 2
        y[] = t
    end
end

## 3D
figure3 = Figure(backgroundcolor = bgc)
ax3 = Axis3(figure3[1,1], aspect = (1,1,1))
ax3.limits = ((-0.1, 1.1), (-0.1, 1.1), (-.1,1.1))
#scatter!(ax3, point3, color = :orange)
bigpoint = @lift Sphere($point3, .05)
mesh!(ax3, bigpoint,color = :orange)


label3 = figure3[2,1] = Label(figure3, text3, textsize = 30, font = "Fira Mono")

position_iter3 = Iterators.flatten((
    Iterators.product(0:dt:1,1:3),
    Iterators.product(1:-dt:0,1:3) ))

record(figure3, "3d.gif", position_iter3, framerate = fps) do (t,i)
    if i == 1
        x[] = t
    elseif i == 2
        y[] = t
    elseif i == 3
        z[] = t
    end
end


## Sphere
figure_sphere = Figure(backgroundcolor = bgc)
ax_sphere = Axis3(figure_sphere[1,1], aspect = (1,1,1))
ax_sphere.limits = ((-1.1, 1.1), (-1.1, 1.1), (-1.1,1.1))
#scatter!(ax_sphere, point3, color = :orange)
surface!(ax_sphere, xs, ys, zs, colormap=(:viridis, 0.5), transparency = true)

bigpoint_s = @lift Sphere($sphere_point, .05)
mesh!(ax_sphere, bigpoint_s,color = :orange)

label_sphere = figure_sphere[2,1] = Label(figure_sphere, text_shpere, textsize = 30, font = "Fira Mono")



position_iter_sphere = Iterators.flatten((
    Iterators.product(0:dθ:π/2,(1)),
    Iterators.product(0:dθ:2*π,(2)),
    [(0.,2)],
    Iterators.product(π/2:-dθ:0,(1))
    ))

record(figure_sphere, "sphere.gif", position_iter_sphere, framerate = fps) do (t,i)
    if i == 1
        θ[] = t
    elseif i == 2
        ϕ[] = t
    end
end





using GLMakie, FileIO, Rotations, CoordinateTransformations
slipper = load("slipper.stl")
scale = .01
fps = 24
dt = 1. / fps 

bgc = RGBf0(0.98, 0.98, 0.98)


# position = Node{Vec3f0(0.,0.,0.)}
x = Node{Float32}(0.0)
y = Node{Float32}(0.0)
z = Node{Float32}(0.0)


θ = Node{Float32}(0.0)
ϕ = Node{Float32}(0.0)
ψ = Node{Float32}(0.0)
position_text = @lift string(
    "x = ", floor($x, digits = 1), ", ",
    "y = ", floor($y, digits = 1), ", ",
    "z = ", floor($z, digits = 1)
    )

angle_text = @lift string(
    "θ = ", lpad(floor(Int, mod($θ, π/2) *180. /π),3), "°, ",
    "ϕ = ", lpad(floor(Int, mod($ϕ, π/2) *180. /π),3), "°, ",
    "ψ = ", lpad(floor(Int, mod($ψ, 2π)  *180. /π),3), "°",
    )


rot_axis = @lift [cos($ϕ) * sin($θ), sin($ϕ) * sin($θ), cos($θ)]

line0 = [-2.,2.]
line_x = @lift([-2., 2.] * $rot_axis[1])
line_y = @lift([-2., 2.] * $rot_axis[2])
line_z = @lift([-2., 2.] * $rot_axis[3])


rot = @lift AngleAxis($ψ, $rot_axis...)

rot_quat = @lift $rot |> UnitQuaternion |> Rotations.params |> q -> Quaternionf0(q[2],q[3],q[4],q[1])


## Figure1
figure = Figure(backgroundcolor = bgc)
ax = figure[1,1] = Axis3(figure, camera = cam3d!)
f=mesh!(ax,slipper,colormap = :viridis,  color = [tri[1][2] for tri in slipper for i in 1:3], showaxis = false)
scale!(f,scale,scale,scale)
ax.limits = ((-2,2),(-2,2),(-2,2))

onany(x, y, z) do x, y, z
     translate!(f, x, y, z)
end

ax_text = figure[2,:]  = Label(figure,
position_text, textsize = 30, font = "Fira Mono")


position_iter = Iterators.flatten((
    Iterators.product(0:dt:1,1:3),
    Iterators.product(1:-dt:0,1:3) ))

record(figure, "rigid_translate.gif", position_iter, framerate = fps) do (t,i)
    if i == 1
        x[] = t
    elseif i == 2
        y[] = t
    elseif i == 3
        z[] = t
    end
end    

## Rotations
figure_rot = Figure(backgroundcolor = bgc)
ax_rot = figure_rot[1,1] = Axis3(figure_rot)
f2=mesh!(ax_rot,slipper,colormap = :viridis,  color = [tri[1][2] for tri in slipper for i in 1:3], showaxis = false)
scale!(f2,scale,scale,scale)
ax_rot.limits = ((-2,2),(-2,2),(-2,2))

on(rot_quat) do rot_quat
     rotate!(f2, rot_quat)
end

lines!(ax_rot,line_x,line_y,line_z,color = :orange, linewidth = 5., linestyle = :dash)

ax_text_rot = figure_rot[2,:]  = Label(figure_rot,
angle_text, textsize = 30, font = "Fira Mono")

dθ = dt * π / 2
rotations_iter = Iterators.flatten((
    Iterators.product(0: dθ: 2π, (3)),
    Iterators.product(0: dθ: π/2, (1)),
    Iterators.product(0: dθ: π/2, (3)),
    Iterators.product(0: dθ: π, (2)),
    Iterators.product(π/2: dθ: 2*π, (1)),
    Iterators.product(π/2: - dθ:0, (1,2,3)),
    ))
    
record(figure_rot, "rigid_rotate.gif", rotations_iter, framerate = fps) do (t,i)
    if i == 1
        θ[] = t
    elseif i == 2
        ϕ[] = t
    elseif i == 3
        ψ[] = t
    end
end    



## Boy

function boy_surface(θ,ϕ)
    k = cos(θ) / (sqrt(2.) - sin(2 * θ) * cos(3 * ϕ) )
    x = k * (cos(θ) * cos(2 * ϕ) + sqrt(2.) * sin(θ) * sin(ϕ)) 
    y = k * (cos(θ) * cos(2 * ϕ) - sqrt(2.) * sin(θ) * sin(ϕ)) 
    z = 3* k * cos(θ)
    return [x,y,z]
end

boy_xs = [boy_surface(a,b)[1] for a in 0:.1:π, b in 0:.1:π]
boy_ys = [boy_surface(a,b)[2] for a in 0:.1:π, b in 0:.1:π]
boy_zs = [boy_surface(a,b)[3] for a in 0:.1:π, b in 0:.1:π]


boy_point = @lift Point3f0(boy_surface($ϕ,$θ)...)
# boy_x = @lift boy[1]
# boy_y = @lift boy[1]
# boy_z = @lift boy[1]

figure_boy = Figure(backgroundcolor = bgc)
ax_slipper = figure_boy[1,1] = Axis3(figure_boy)



f3=mesh!(ax_slipper,slipper,colormap = :viridis,  color = [tri[1][2] for tri in slipper for i in 1:3], showaxis = false)
scale!(f3,scale,scale,scale)
ax_slipper.limits = ((-2,2),(-2,2),(-2,2))

on(rot_quat) do rot_quat
     rotate!(f3, rot_quat)
end

lines!(ax_slipper,line_x,line_y,line_z,color = :orange, linewidth = 5., linestyle = :dash)



ax_boy  = figure_boy[1,2] = Axis3(figure_boy, aspect = (1,1,1))

surface!(ax_boy, boy_xs, boy_ys, boy_zs; colormap=(:viridis, 0.6), transparency = true)

bigpoint = @lift Sphere($boy_point, .1)
#scatter!(ax_boy, boy_point, color = :orange, markersize = 1.)
mesh!(ax_boy, bigpoint,color = :orange)

ax_text_rot_boy = figure_boy[2,:]  = Label(figure_boy,
angle_text, textsize = 30, font = "Fira Mono")

record(figure_boy, "rigid_rotate_3d.gif", rotations_iter, framerate = fps) do (t,i)
    if i == 1
        θ[] = t
    elseif i == 2
        ϕ[] = t
    elseif i == 3
        ψ[] = t
    end
end    


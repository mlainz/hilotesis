using DifferentialEquations, GLMakie, GeometryBasics

bgc = RGBf0(0.98, 0.98, 0.98)

# grids = (xminorticksvisible = true,
# yminorticksvisible = true,
# xminorgridvisible = true,
# yminorgridvisible = true)
# Initial conditions
#p₀ = 2.5
p₀ = 1.0

# params
const g = 1.0
const θ₀ = 0.
fps = 24
T = (0., 10.)
dt= 1. / fps
const step = .01 * dt

# Define Hamiltonian 
H(p::Real,θ::Real,params) = p^2/2 + g * (1-cos(θ))
XH(θ::Real,p::Real) = Point2(p,  - g*sin(θ))
function XH3D(p0)
    x,y,p = p0[:]
    Point3(-y*p/8, x*p/8,  - g * y / sqrt(x^2+y^2)/8)
end


#Solve PDE
prob = HamiltonianProblem(H, p₀, θ₀, T)
sol = solve(prob, McAte5(), dt = step)

# Compute period
n_max = if abs(p₀) < 2 #bounded case
    2* findfirst([sign(p₀) * s[2] < 0 for s in sol[:]])
else
    findfirst([s[2] >= 2. * π for s in sol[:]])
end

T = (0., step * n_max)


t = Node(T[1])
s0 = @lift(sol($t))
θ = @lift(mod2pi($s0[2] + π) - π)
p = @lift($s0[1])
θ_deg = @lift $θ*180/π

timestamps = T[1]:dt:T[2]

text_position = @lift string(
    "θ = ", lpad(floor(Int,$θ_deg), 4), "°, ",
    "v = ", lpad(floor($p, digits = 1),4))
    
    x= @lift(sin($θ))
y = @lift( - cos($θ))
p_x = @lift( $p*cos($θ)) 
p_y = @lift( $p*sin($θ))
θs = -1.1 * π : 0.1 : 1.1 * π
ps = 1. : 0.2 : 3.
xs = [cos(θ) for θ in θs, z in ps]
ys = [sin(θ) for θ in θs, z in ps]
zs = [z for θ in θs, z in ps]
poitns_cylinder = vec(Point3f0.(xs,ys,zs))

point = @lift(Point2f0($x,$y))

s_point = @lift(Point2f0($θ,$p))
point_3d = @lift(Point3f0($x,$y,$p))


##PLOTS
##Just the pendulum
figure0 = Figure(backgroundcolor = bgc)

#Pendulum
ax0 = figure0[1,1] = Axis(figure0, 
    aspect = AxisAspect(1),
    xlabel = "x", ylabel = "y")
    
limits!(ax0, -1.2, 1.2, -1.2, 1.2)

# circle
lines!(ax0,cos.(θs),sin.(θs),
        color=:black,
        linestyle=:dash)
        
# point
scatter!(ax0,point,  color = :orange
)

#string
lx = @lift([0,$x])
ly = @lift([0,$y])
lines!(ax0, lx,ly)

# Center axis
lines!(ax0, [-1.2,1.2],[0,0], color = :black, linestyle = :dash)
lines!(ax0, [0,0],[-1.2,1.2], color = :black, linestyle = :dash)


# velocity arrow
p_vec = @lift([Vec2f0($p_x/4,$p_y/4)])
x_vec= @lift([$point])
arrows!(ax0,x_vec,p_vec; arrowsize = .1)

#Legend
ax_text0 = figure0[2,:]  = Label(figure0,
text_position, textsize = 30., font = "Fira Mono")

record(figure0, "pendulum0_$p₀.gif", timestamps; framerate = fps) do tᵢ
    t[] = tᵢ
end    


##Pendulum and phase plot
figure = Figure(backgroundcolor = bgc)

#Pendulum
ax1 = figure[1,1] = Axis(figure, 
    aspect = AxisAspect(1),
    xlabel = "x", ylabel = "y")
    
limits!(ax1, -1.2, 1.2, -1.2, 1.2)

# circle
lines!(ax1,cos.(θs),sin.(θs),
        color=:black,
        linestyle=:dash)
        
# point
scatter!(ax1,point,  color = :orange
)

#string
lx = @lift([0,$x])
ly = @lift([0,$y])
lines!(ax1, lx,ly)

# Center axis
lines!(ax1, [-1.2,1.2],[0,0], color = :black, linestyle = :dash)
lines!(ax1, [0,0],[-1.2,1.2], color = :black, linestyle = :dash)


# velocity arrow
p_vec = @lift([Vec2f0($p_x/4,$p_y/4)])
x_vec= @lift([$point])
arrows!(ax1,x_vec,p_vec; arrowsize = .1)


#Phase space
ax2 = figure[1,2] = Axis(figure, 
    aspect = AxisAspect(1),
    xlabel = "θ",
    ylabel = "v",
    xticks = (-π:π/2:π, string.(-180:90:180) .* "°"))

limits!(ax2, θs[1],θs[end],-3,3)


streamplot!(ax2, XH, θs, -3..3, 
gridsize= (32,32), arrow_size = 0.05)

# Center axis
lines!(ax2, [θs[1],θs[end]],[0,0], color = :black, linestyle = :dash)
lines!(ax2, [0,0],[-3,3], color = :black, linestyle = :dash)

scatter!(ax2, s_point,  color = :orange)



#Legend
ax_text = figure[2,:]  = Label(figure,
text_position, textsize = 30., font = "Fira Mono")

record(figure, "pendulum_$p₀.gif", timestamps; framerate = fps) do tᵢ
    t[] = tᵢ
end    

## Second plot

figure2 = Figure(backgroundcolor = bgc)

#Pendulum
ax1b = figure2[1,1] = Axis(figure2, 
    aspect = AxisAspect(1),
    xlabel = "x", ylabel = "y" )
limits!(ax1b, -1.2, 1.2, -1.2, 1.2)

# circle
lines!(ax1b,cos.(θs),sin.(θs),
        color=:black,
        linestyle=:dash)
        
# point
scatter!(ax1b,point,  color = :orange
)

#string
lx = @lift([0,$x])
ly = @lift([0,$y])
lines!(ax1b, lx,ly)

# velocity arrow
p_vec = @lift([Vec2f0($p_x/4,$p_y/4)])
x_vec= @lift([$point])
arrows!(ax1b,x_vec,p_vec; arrowsize = .1)

# Center axis
lines!(ax1b, [-1.2,1.2],[0,0], color = :black, linestyle = :dash)
lines!(ax1b, [0,0],[-1.2,1.2], color = :black, linestyle = :dash)



ax3  = figure2[1,2] = Axis3(figure2,
                     zlabel = "v", aspect = (1,1,1))

surface!(ax3, xs, ys, zs, colormap=(:viridis, 0.5), transparency = true)
bigpoint = @lift Sphere($point_3d, .1)
#scatter!(ax3, point_3d, color = :orange, markersize = 500)
mesh!(ax3, bigpoint,color = :orange)

arrows!(ax3, poitns_cylinder, XH3D, arrowsize = 0.03)

#Legend
ax_text2 = figure2[2,:]  = Label(figure2,
text_position, textsize = 30, font = "Fira Mono")

record(figure2, "pendulum3d_$p₀.gif", timestamps; framerate = fps) do tᵢ
    t[] = tᵢ
end    


#TODO change font, prevent numbers from moving, add middle ticks, reverse cylinder axis, plot stream in cylinder, delete "double surface", bigger dot, limits
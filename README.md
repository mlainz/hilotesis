# Guion twitter

1. ¿Cuántas dimensiones necesitas para lanzar una zapatilla? Me uno al reto de @RedDivulga, a @UAM_Madrid, @UCCUAM, @EDoctorado_UAM. Abro #HiloTesis [encuesta].

2. Las dimensiones de un objeto es el mínimo número de coordenadas que hacen falta para determinar un punto. Así, una recta tendría una dimesión, un plano dos y el espacio tres. ![](1d.gif) ![](2d.gif) ![](3d.gif)

3. La dimesión de un objeto sólo depende del objeto en sí, no del espacio donde esté metido. Una curva siempre tiene dimensión 1, da igual que esté en el plano o en el espacio. Por ejemplo, se puede localizar un lugar en una carretera  por el km en el que está (1 sola coordenada).![](line.gif)

4. Del mismo modo, cualquier superficie tiene dimensión dos. Por ejemplo, los puntos de la superficie de una esfera pueden especificarse con su latitud θ y longitud ϕ. ![](sphere.gif)


5. También existen objetos de más de tres dimensiones. Por ejemplo el hipercubo (también llamado teseracto) es el análogo al cubo en 4d. Aunque no podemos visualizarlo directamente, si podemos ver su proyección (su «sombra») en el espacio de 3 dimensiones. ![](https://upload.wikimedia.org/wikipedia/commons/d/d9/From_Point_to_Tesseract_%28Looped_Version%29.gif)

6. ¿Qué tiene qué ver esto con la zapatilla? <br>Para entenderlo mejor, empezaremos por un ejemplo más sencillo: un péndulo. ![](walter-levin.gif)

7. Conocido el ángulo θ y la velocidad v del péndulo en un instante, las leyes de la física nos permiten determinar su evolución en cualquier momento. Por lo tanto, estas dos «coordenadas» podemos describir el estado del péndulo. Se puede considerar un objeto de dimensión 2. ![](pendulum0_1.0.gif)

8. Representando la posición θ y la velocidad v del péndulo, vemos que cada trayectoria se corresponde con una curva en el plano. <br>
El péndulo comienza a θ = 0° (abajo) y avanza con v positiva (antihoraria). Va subiendo hasta alcanzar su altura máxima (θ = 60°). ![](pendulum_1.0.gif)

9. Mientras, v va decreciendo y llega a 0 cuando el péndulo está arriba. Posteriormente, v toma valores negativos, mientras el péndulo desciende. Al llegar abajo (θ = 0°) se vuelve al principio, pero con v negativa. <br> El proceso se repite ahora una vez el otro sentido, y volvemos a empezar.![](pendulum_1.0.gif)

10. Si la v inicial es alta, el péndulo avanzará siempre en sentido antihorario. v será mayor abajo y menor arriba. Aunque la trayectoria del péndulo real es continua, en el plano de la derecha pega un salto y pasa instantanemente de +180° a -180°. ![](pendulum_2.5.gif)


11. Para arreglarlo, «pegamos» los dos lados del plano, por las lineas verticales con θ = ± 180°, obteniendo un cilindro. Con esta representación, un cambio pequeño en el estado del péndulo se corresponde a un movimiento pequeño en el cilindro, sin saltos extraños. ![](pendulum3d_2.5.gif)

12. Esta representación «continua» de las posibles posiciones y velocidades de un sistema físico se llama «espacio de fases». Dicho espacio es una figura geométrica que puede tener más de tres dimensiones y curvarse de forma enrevesada. ![](escher-relativity.jpg)

13. Entonces ¿qué forma tiene el espacio de fases de la zapatilla?  <br>
Para comenzar, describiremos su posición en el espacio. Para ello, necesitamos 3 coordenadas (x,y,z). ![](rigid_translate.gif)

14. También usaremos 3 coordenadas para el estado de rotación. Euler demostró que toda rotación puede describirse dando un eje de rotación y el ángulo ψ que se rota alrededor del eje. El eje puede definirse a partir de su «latitud» θ y «longitud» ϕ.
![](rigid_rotate.gif) 

15. Por último, tenemos 6 posibles velocidades (3 espaciales y 3 de giro). En total, son 12 coordenadas. Es decir, el espacio de fases de la zapatilla tiene ¡12 dimensiones!. Sí, a pesar de que la zapatilla esté en el espacio, necesitamos 12 números para describirla. ![](audrey_slipper.gif)

16. ¿Y que forma tiene tiene este objeto de 12 dimensiones? Pues esto ya es más complejo. Se llama ℝ⁹×ℝP³. Puede descomponerse en dos partes, ℝ⁹, que es el espacio  «normal», pero de 9 dimensiones en lugar de 3. Estas son las 3 posiciones espaciales y las 6 velocidades. 

17. La otra, ℝP³, representa las 3 dimensiones de rotación. A la derecha representamos una proyección de ℝP³ (llamada ℝP²), que se corresponde a las posibles posiciones del eje de rotación. Esto es, este espacio tan complicado, ℝ⁹×ℝP³, es a la zapatilla lo que el cilindro al péndulo.
![](rigid_rotate_3d.gif)

18. Por último, ¿para qué sirve esto y qué tiene que ver con mi tesis?<br>
Mi tesis va sobre la geometría de contacto, que se enmarca en la mecánica geométrica. Esta área de las matemáticas estudia las propiedades de  sistema físicos a partir de la geometría de sus espacios de fases.

19. La geometría de contacto aparece en sistemas con fricción o rozamiento. Se aplica a problemas de física, ingeniería y robótica. Buscamos nuevos sitwmas que puedan estudiarse con esta geometría, y desarrollamos herramientas teóricas para analizarlos y comprender su funcionamiento.

20. En concreto, hemos demostrado que las simetrías de estos espacios de fases están asociadas a cantidades que se disipan en el tiempo y hemos desarrollado unos integradores que permiten simular la evolución de estos sistemas con un ordenador, preservando su estructura geométrica.